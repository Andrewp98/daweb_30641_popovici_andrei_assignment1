import React, { useState, useContext, createContext } from "react";
import { useTranslation } from "react-i18next";
import { Form, FormGroup, Label, Input, Col, FormText } from "reactstrap";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import D1 from "./img/Docs/d1.jpg";
import axios from "axios";
import { useUserContext } from "./hooks/UserContext";
import { useHistory } from "react-router-dom";
import Alert from "react-bootstrap/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import { CalendarUser } from "./calendar-component";

export default function Userpage() {
  const [gdpr, setGdpr] = useState([]);

  const [id, setId] = useState(0);
  const [username, setUsername] = useState([]);
  const [email, setEmail] = useState([]);
  const [password, setPassword] = useState([]);
  const [passwordC, setPasswordC] = useState([]);

  const [emailValid, setEmailValid] = useState(false);
  const [passwordValid, setPasswordValid] = useState(false);
  const [passwordCValid, setPasswordCValid] = useState(false);
  const { t, i18n } = useTranslation();
  const { state: userState } = useUserContext();
  const history = useHistory();
  const [open, setOpen] = useState(false);

  const [selectedDate, setSelectedDate] = useState(new Date());
  const selectedDateChange = (date) => {
    setSelectedDate(date);
  };
  const [timeslot, setTimeslot] = useState(0);
  const [timeslotType, setTimeslotType] = useState(["1", "2", "3", "4"]);

  const { dispatch } = useUserContext();

  const [servtype, setServtype] = useState([
    "Albire dinti",
    "Extractie masea de minte",
    "Detartraj",
    "Implant",
    "Radiografie",
  ]);
  const servs = [];
  var medications = "";
  const Add = servtype.map((Add) => Add);
  const AddTimeslot = timeslotType.map((AddTimeslot) => AddTimeslot);

  const handleFileUpload = (e) => {
    // console.log(e.target.files[0]);
    setGdpr([...gdpr, e.target.files[0]]);
    // console.log(userState.user?.token);
    console.log(gdpr);
  };

  const sendFile = () => {
    // console.log(gdpr);
    const fdata = new FormData();
    console.log(gdpr);
    console.log(document.getElementById("file").files[0]);
    // fdata.append("file", document.getElementById("file").files[0]);
    fdata.append("file", ...gdpr);

    axios({
      method: "post",
      responseType: "json",
      url: "http://localhost:8000/upload-file",
      data: fdata,
      headers: { "Content-Type": "multipart/form-data" },
    })
      .then(function (response) {
        //handle success
        console.log(response);
      })
      .catch((error) => {
        console.log("ERRRR:: ", error.response.data);
      });
  };

  const makeAppiontent = () => {
    axios
      .post("http://localhost:8000/api/createAppoint", {
        doctorId: 3,
        patientId: userState.user?.id,
        appDate: selectedDate.toISOString().substring(0, 10),
        timeslot: timeslot,
      })
      .then((res) => {
        console.log(res.data.data);
      })
      .catch((err) => console.log(err));
    // console.log(
    //   "PatientID: " +
    //     userState.user?.id +
    //     " appDate: " +
    //     selectedDate.toISOString().substring(0, 10) +
    //     " timeslot: " +
    //     timeslot
    // );
  };

  const handleServTypeChange = (e) => {
    if (servs.includes(servtype[e.target.value])) {
      console.log("Already in the array!");
    } else {
      servs.push(servtype[e.target.value]);
      medications = medications + servtype[e.target.value] + ",";
    }
  };

  const handleTimeslot = (value) => {
    console.log(Number(value.target.value) + 1);
    setTimeslot(Number(value.target.value) + 1);
  };

  const handleEmailChange = (value) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(String(value).toLowerCase())) {
      setEmailValid(true);
      setEmail(value);
    } else {
      setEmailValid(false);
      setEmail(value);
    }
  };

  const handleNameChange = (value) => {
    setId(userState.user?.id);
    setUsername(value);
  };

  const handlePasswordChange = (name, value) => {
    if (name === "password") {
      if (value.length >= 5) {
        setPasswordValid(true);
        setPassword(value);
      } else {
        setPasswordValid(false);
        setPassword(value);
      }
    }
    if (name === "confirmPassword") {
      if (value === password) {
        setPasswordCValid(true);
        setPasswordC(value);
      } else {
        setPasswordCValid(false);
        setPasswordC(value);
      }
    }
  };

  const handleUpdate = () => {
    axios
      .post("http://localhost:8000/api/update", {
        id: id,
        name: username,
        email: email,
        password: password,
        medications: medications,
      })
      .then((res) => {
        console.log(res.data.data);
        dispatch({ type: "login", payload: res.data.data });
      });
  };

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <div className="medici">
      <header class="page-header gradient">
        <div class="container pt-3">
          <div class="col-md-10">
            <h2 class="my-4">Bine ai venit {userState.user?.name} !</h2>
            <p>Aceasta este pagina ta de utilizator.</p>
          </div>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fillOpacity="1"
            d="M0,64L40,85.3C80,107,160,149,240,160C320,171,400,149,480,122.7C560,96,640,64,720,90.7C800,117,880,203,960,213.3C1040,224,1120,160,1200,128C1280,96,1360,96,1400,96L1440,96L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"
          ></path>
        </svg>
      </header>

      <section class="services">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-5">
              <img src={D1} alt="" class="img-fluid" />
              <div className="mb-3 mt-3 allign-itmes-right" id="here">
                <FormGroup>
                  <Input
                    type="file"
                    name="file"
                    id="file"
                    onChange={(e) => handleFileUpload(e)}
                  />
                </FormGroup>
                <FormText color="muted">
                  Incarca aici fisierul de consimțământ GDPR.
                </FormText>
              </div>
              <div className="mb-3 mt-3 allign-itmes-right" id="here">
                <FormGroup>
                  <Label for="exampleSelect">
                    Selectati serviciile dorite:
                  </Label>
                  <Input
                    type="select"
                    name="select"
                    id="exampleSelect"
                    onChange={(e) => handleServTypeChange(e)}
                  >
                    {Add.map((address, key) => (
                      <option key={key} value={key}>
                        {address}
                      </option>
                    ))}
                  </Input>
                </FormGroup>
              </div>
            </div>
            <div class="col-md-5">
              <div className="mb-3 allign-items-left " id="here">
                <Label for="usernameField" sm={2}>
                  {" "}
                  Username{" "}
                </Label>
                <Input
                  type="text"
                  name="username"
                  placeholder="Enter your username"
                  id="usernameField"
                  onChange={(e) => handleNameChange(e.target.value)}
                />

                <div className="mb-3 allign-items-left " id="here">
                  <Label for="emailField" sm={2}>
                    {" "}
                    Email{" "}
                  </Label>
                  <Input
                    type="email"
                    name="email"
                    placeholder="email@gmail.com"
                    id="emailField"
                    onChange={(e) => handleEmailChange(e.target.value)}
                    invalid={!emailValid && email !== ""}
                    valid={emailValid && email !== ""}
                  />
                </div>

                <div className="mb-3" id="here">
                  <Label for="passwordField" sm={2}>
                    {" "}
                    Password{" "}
                  </Label>
                  <Input
                    type="password"
                    name="password"
                    placeholder="Enter password"
                    id="passwordField"
                    onChange={(e) =>
                      handlePasswordChange(e.target.name, e.target.value)
                    }
                    invalid={
                      !passwordValid &&
                      password !== "" &&
                      passwordC !== password
                    }
                    valid={
                      passwordValid && password !== "" && passwordC === password
                    }
                  />
                </div>

                <div className="mb-3" id="here">
                  <Label for="passwordCField" sm={2}>
                    {" "}
                    Password Confirmation{" "}
                  </Label>
                  <Input
                    type="password"
                    name="confirmPassword"
                    placeholder="Confirm password"
                    id="passwordCField"
                    onChange={(e) =>
                      handlePasswordChange(e.target.name, e.target.value)
                    }
                    invalid={
                      !passwordCValid &&
                      passwordC !== "" &&
                      passwordC !== password
                    }
                    valid={
                      passwordValid && password !== "" && passwordC === password
                    }
                  />
                </div>

                <button
                  type="button"
                  className="btn btn-outline-warning mb-3"
                  onClick={() => {
                    handleUpdate();
                    handleClick();
                  }}
                >
                  Update
                </button>

                <button
                  type="button"
                  className="btn btn-outline-primary mb-3 mx-3"
                  onClick={sendFile}
                >
                  Upload File
                </button>

                <button
                  type="button"
                  className="btn btn-outline-info mb-3 mx-3"
                  onClick={makeAppiontent}
                >
                  Make appointment
                </button>

                <Snackbar
                  open={open}
                  autoHideDuration={6000}
                  onClose={handleClose}
                >
                  <Alert onClose={handleClose} severity="success">
                    Hey, {userState.user?.name} your profile has been updated!
                  </Alert>
                </Snackbar>
              </div>
            </div>
          </div>

          <div class="row align-items-center justify-content-center">
            <div class="col-md-5">
              <div className="mb-3 mt-3 allign-itmes-right" id="here">
                <FormGroup>
                  <Label for="exampleSelect">
                    Selectati o data pentru programare:
                  </Label>
                  <CalendarUser
                    id="calendarUser"
                    selected={selectedDate}
                    onSelectedChange={selectedDateChange}
                  />
                </FormGroup>
              </div>
            </div>
            <div class="col-md-5">
              <FormGroup>
                <Label for="timeslot">Selectati un interval orar:</Label>
                <Input
                  type="select"
                  name="select"
                  id="timeslot"
                  onChange={(value) => handleTimeslot(value)}
                >
                  {AddTimeslot.map((address, key) => (
                    <option key={key} value={key}>
                      {address}
                    </option>
                  ))}
                </Input>
              </FormGroup>
            </div>
          </div>
        </div>
      </section>

      <footer class="page-footer gradient">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fillOpacity="1"
            d="M0,0L40,21.3C80,43,160,85,240,117.3C320,149,400,171,480,176C560,181,640,171,720,149.3C800,128,880,96,960,74.7C1040,53,1120,43,1200,48C1280,53,1360,75,1400,85.3L1440,96L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          ></path>
        </svg>
      </footer>
    </div>
  );
}
