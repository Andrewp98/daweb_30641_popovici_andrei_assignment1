import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export const CalendarUser = ({ selected, onSelectedChange }) => {
  const [date, setDate] = useState(selected);

  useEffect(() => {
    setDate(selected);
  }, [selected]);

  return (
    <DatePicker selected={date} onChange={(date) => onSelectedChange(date)} />
  );
};
