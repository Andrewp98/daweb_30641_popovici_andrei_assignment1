import React, { useState } from "react";
import { Form, FormGroup, Label, Input, Col } from "reactstrap";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import { Context } from "./context";
import axios from "axios";
import { useUserContext } from "./hooks/UserContext";

export default function SignUp() {
  const [username, setUsername] = useState([]);
  const [email, setEmail] = useState([]);
  const [password, setPassword] = useState([]);
  const [passwordC, setPasswordC] = useState([]);

  const [emailValid, setEmailValid] = useState(false);
  const [passwordValid, setPasswordValid] = useState(false);
  const [passwordCValid, setPasswordCValid] = useState(false);

  const { dispatch } = useUserContext();

  const handleEmailChange = (value) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(String(value).toLowerCase())) {
      setEmailValid(true);
      setEmail(value);
    } else {
      setEmailValid(false);
      setEmail(value);
    }
  };

  const handleNameChange = (value) => {
    setUsername(value);
  };

  const handlePasswordChange = (name, value) => {
    if (name === "password") {
      if (value.length >= 5) {
        setPasswordValid(true);
        setPassword(value);
      } else {
        setPasswordValid(false);
        setPassword(value);
      }
    }
    if (name === "confirmPassword") {
      if (value === password) {
        setPasswordCValid(true);
        setPasswordC(value);
      } else {
        setPasswordCValid(false);
        setPasswordC(value);
      }
    }
  };

  const handleSubmit = () => {
    axios
      .post("http://localhost:8000/api/register", {
        name: username,
        email: email,
        password: password,
        c_password: passwordC,
      })
      .then((res) => {
        console.log(res.data.data);
        // dispatch({ type: "login", payload: res.data.data });
      });
  };

  return (
    <div className="signup">
      <header className="page-header gradient">
        <div className="container pt-3">
          <div className="row allign-items-center justify-content-center"></div>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#ffffff"
            fillOpacity="1"
            d="M0,288L48,272C96,256,192,224,288,218.7C384,213,480,235,576,202.7C672,171,768,85,864,69.3C960,53,1056,107,1152,117.3C1248,128,1344,96,1392,80L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
          ></path>
        </svg>
      </header>

      <section className="signupForm">
        <div className="container text-left">
          <div className="row allign-items-center justify-content-center">
            <div className="bg-white shadow-md rounded w-50 px-8 pt-6 pb-8 mb-4 mt-4 border-gray-200 border">
              <Form>
                <div className="mb-4 mt-4 ">
                  <h1>Create new Account</h1>

                  <FormGroup row>
                    <div className="mb-3 allign-items-left " id="here">
                      <Label for="usernameField" sm={2}>
                        {" "}
                        Username{" "}
                      </Label>
                      <Input
                        type="text"
                        name="username"
                        placeholder="Enter your username"
                        id="usernameField"
                        onChange={(e) => handleNameChange(e.target.value)}
                      />
                    </div>
                  </FormGroup>

                  <FormGroup row>
                    <div className="mb-3 allign-items-left " id="here">
                      <Label for="emailField" sm={2}>
                        {" "}
                        Email{" "}
                      </Label>
                      <Input
                        type="email"
                        name="email"
                        placeholder="email@gmail.com"
                        id="emailField"
                        onChange={(e) => handleEmailChange(e.target.value)}
                        invalid={!emailValid && email !== ""}
                        valid={emailValid && email !== ""}
                      />
                    </div>
                  </FormGroup>

                  <FormGroup row>
                    <div className="mb-3" id="here">
                      <Label for="passwordField" sm={2}>
                        {" "}
                        Password{" "}
                      </Label>
                      <Input
                        type="password"
                        name="password"
                        placeholder="Enter password"
                        id="passwordField"
                        onChange={(e) =>
                          handlePasswordChange(e.target.name, e.target.value)
                        }
                        invalid={
                          !passwordValid &&
                          password !== "" &&
                          passwordC !== password
                        }
                        valid={
                          passwordValid &&
                          password !== "" &&
                          passwordC === password
                        }
                      />
                    </div>
                  </FormGroup>

                  <FormGroup row>
                    <div className="mb-3" id="here">
                      <Label for="passwordCField" sm={2}>
                        {" "}
                        Password Confirmation{" "}
                      </Label>
                      <Input
                        type="password"
                        name="confirmPassword"
                        placeholder="Confirm password"
                        id="passwordCField"
                        onChange={(e) =>
                          handlePasswordChange(e.target.name, e.target.value)
                        }
                        invalid={
                          !passwordCValid &&
                          passwordC !== "" &&
                          passwordC !== password
                        }
                        valid={
                          passwordValid &&
                          password !== "" &&
                          passwordC === password
                        }
                      />
                    </div>
                  </FormGroup>

                  <div className="mb-3">
                    <button
                      type="button"
                      className="btn btn-primary position-relative"
                      onClick={handleSubmit}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </section>

      <footer className="page-footer gradient">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fillOpacity="1"
            d="M0,0L40,21.3C80,43,160,85,240,117.3C320,149,400,171,480,176C560,181,640,171,720,149.3C800,128,880,96,960,74.7C1040,53,1120,43,1200,48C1280,53,1360,75,1400,85.3L1440,96L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          ></path>
        </svg>
      </footer>
    </div>
  );
}
