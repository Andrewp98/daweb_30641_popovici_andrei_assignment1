import React, { useState, useEffect, useContext, createContext } from "react";

import { useTranslation } from "react-i18next";
import {
  Form,
  FormGroup,
  Label,
  Input,
  Col,
  FormText,
  Table,
} from "reactstrap";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import D1 from "./img/Docs/d1.jpg";
import axios from "axios";
import { useUserContext } from "./hooks/UserContext";
import { useHistory } from "react-router-dom";
import Alert from "react-bootstrap/Alert";
import Snackbar from "@material-ui/core/Snackbar";

import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

import InfiniteCalendar, {
  Calendar,
  defaultMultipleDateInterpolation,
  withMultipleDates,
} from "react-infinite-calendar";
import "react-infinite-calendar/styles.css";

export default function Userpage() {
  const [id, setId] = useState(0);
  const [username, setUsername] = useState([]);
  const [email, setEmail] = useState([]);
  const [password, setPassword] = useState([]);
  const [passwordC, setPasswordC] = useState([]);

  const [emailValid, setEmailValid] = useState(false);
  const [passwordValid, setPasswordValid] = useState(false);
  const [passwordCValid, setPasswordCValid] = useState(false);
  const { t, i18n } = useTranslation();
  const { state: userState } = useUserContext();
  const history = useHistory();
  const [open, setOpen] = useState(false);

  const [appointData, setAppointData] = useState([]);

  const [dates, setDates] = useState([""]);

  const { dispatch } = useUserContext();

  const MultipleDatesCalendar = withMultipleDates(Calendar);

  useEffect(() => {
    recDates();
    recApp();
  }, []);

  const recApp = () => {
    const urlText =
      "http://localhost:8000/api/getAppoints/" + userState.user?.id;

    axios({
      method: "get",
      url: urlText,
    })
      .then(function (response) {
        //handle success
        // response.data.data.map((entry) => {
        //   // console.log(entry.patientId + " " + entry.appDate)
        // });
        setAppointData(response.data.data);
        console.log(appointData);
      })
      .catch((error) => {
        console.log("ERRRR:: ", error.response.data);
      });
  };

  const recDates = () => {
    const urlText = "http://localhost:8000/api/getDates/" + userState.user?.id;

    axios({
      method: "get",
      url: urlText,
    })
      .then(function (response) {
        //handle success
        setDates(response.data.data);
      })
      .catch((error) => {
        console.log("ERRRR:: ", error.response.data);
      });
  };

  const handleEmailChange = (value) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(String(value).toLowerCase())) {
      setEmailValid(true);
      setEmail(value);
    } else {
      setEmailValid(false);
      setEmail(value);
    }
  };

  const handleNameChange = (value) => {
    setId(userState.user?.id);
    setUsername(value);
  };

  const handlePasswordChange = (name, value) => {
    if (name === "password") {
      if (value.length >= 5) {
        setPasswordValid(true);
        setPassword(value);
      } else {
        setPasswordValid(false);
        setPassword(value);
      }
    }
    if (name === "confirmPassword") {
      if (value === password) {
        setPasswordCValid(true);
        setPasswordC(value);
      } else {
        setPasswordCValid(false);
        setPasswordC(value);
      }
    }
  };

  const handleUpdate = () => {
    axios
      .post("http://localhost:8000/api/update", {
        id: id,
        name: username,
        email: email,
        password: password,
        medications: "",
      })
      .then((res) => {
        console.log(res.data.data);
        dispatch({ type: "login", payload: res.data.data });
      });
  };

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const handleSwitch = (param) => {
    switch (param) {
      case 1:
        return "10:00-12:00";
      case 2:
        return "12:00-14:00";
      case 3:
        return "14:00-16:00";
      case 4:
        return "16:00-18:00";
    }
  };

  return (
    <div className="medici">
      <header class="page-header gradient">
        <div class="container pt-3">
          <div class="col-md-10">
            <h2 class="my-4">Bine ati venit DR.{userState.user?.name} !</h2>
            <p>Aceasta este pagina ta de utilizator.</p>
          </div>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fillOpacity="1"
            d="M0,64L40,85.3C80,107,160,149,240,160C320,171,400,149,480,122.7C560,96,640,64,720,90.7C800,117,880,203,960,213.3C1040,224,1120,160,1200,128C1280,96,1360,96,1400,96L1440,96L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"
          ></path>
        </svg>
      </header>

      <section class="services">
        <div class="container">
          <div class="row align-items-center justify-content-center mb-3">
            <div class="col-md-5">
              <div className="mb-3 allign-items-left " id="here">
                <Label for="usernameField" sm={2}>
                  {" "}
                  Username{" "}
                </Label>
                <Input
                  type="text"
                  name="username"
                  placeholder="Enter your username"
                  id="usernameField"
                  onChange={(e) => handleNameChange(e.target.value)}
                />

                <div className="mb-3 allign-items-left " id="here">
                  <Label for="emailField" sm={2}>
                    {" "}
                    Email{" "}
                  </Label>
                  <Input
                    type="email"
                    name="email"
                    placeholder="email@gmail.com"
                    id="emailField"
                    onChange={(e) => handleEmailChange(e.target.value)}
                    invalid={!emailValid && email !== ""}
                    valid={emailValid && email !== ""}
                  />
                </div>

                <div className="mb-3" id="here">
                  <Label for="passwordField" sm={2}>
                    {" "}
                    Password{" "}
                  </Label>
                  <Input
                    type="password"
                    name="password"
                    placeholder="Enter password"
                    id="passwordField"
                    onChange={(e) =>
                      handlePasswordChange(e.target.name, e.target.value)
                    }
                    invalid={
                      !passwordValid &&
                      password !== "" &&
                      passwordC !== password
                    }
                    valid={
                      passwordValid && password !== "" && passwordC === password
                    }
                  />
                </div>

                <div className="mb-3" id="here">
                  <Label for="passwordCField" sm={2}>
                    {" "}
                    Password Confirmation{" "}
                  </Label>
                  <Input
                    type="password"
                    name="confirmPassword"
                    placeholder="Confirm password"
                    id="passwordCField"
                    onChange={(e) =>
                      handlePasswordChange(e.target.name, e.target.value)
                    }
                    invalid={
                      !passwordCValid &&
                      passwordC !== "" &&
                      passwordC !== password
                    }
                    valid={
                      passwordValid && password !== "" && passwordC === password
                    }
                  />
                </div>

                <button
                  type="button"
                  className="btn btn-outline-primary mx-3"
                  onClick={() => {
                    handleUpdate();
                    handleClick();
                  }}
                >
                  Update
                </button>

                <button
                  type="button"
                  className="btn btn-outline-secondary mx-3"
                  onClick={() => {
                    history.push("/docStats");
                  }}
                >
                  See statistics
                </button>

                <Snackbar
                  open={open}
                  autoHideDuration={6000}
                  onClose={handleClose}
                >
                  <Alert onClose={handleClose} severity="success">
                    Hey, {userState.user?.name} your profile has been updated!
                  </Alert>
                </Snackbar>
              </div>
            </div>

            {/* Between sides */}

            <div class="col-md-5">
              <InfiniteCalendar
                width={400}
                height={300}
                Component={MultipleDatesCalendar}
                interpolateSelection={defaultMultipleDateInterpolation}
                selected={[new Date(), ...dates.map((date) => new Date(date))]}
              />
            </div>
          </div>

          <div className="row align-items-center justify-content-center mx-3">
            <Table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Patient ID</th>
                  <th>Date</th>
                  <th>Timeslot</th>
                </tr>
              </thead>
              <tbody>
                {appointData.map((item) => (
                  <tr>
                    <th scope="row">{item.id}</th>
                    <td>{item.patientId}</td>
                    <td>{item.appDate}</td>
                    <td>{handleSwitch(item.timeslot)}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      </section>

      <footer class="page-footer gradient">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fillOpacity="1"
            d="M0,0L40,21.3C80,43,160,85,240,117.3C320,149,400,171,480,176C560,181,640,171,720,149.3C800,128,880,96,960,74.7C1040,53,1120,43,1200,48C1280,53,1360,75,1400,85.3L1440,96L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          ></path>
        </svg>
      </footer>
    </div>
  );
}
