import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import Mark from "./img/mark.svg";
import Abt from "./img/about.jpg";

export default function Despre() {
  const { t, i18n } = useTranslation();

  return (
    <div className="despre">
      <header class="page-header gradient">
        <div class="container pt-3">
          <div class="row allign-items-center justify-content-center">
            <div class="col-md-5">
              <img src={Mark} alt="Header image" />
            </div>
            <div class="col-md-5">
              <h2 class="my-4">{t("Despre1.t")}</h2>

              <p>{t("Despre2.t")}</p>
              <p>{t("Despre3.t")}</p>
            </div>
          </div>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fill-opacity="1"
            d="M0,128L13.3,117.3C26.7,107,53,85,80,112C106.7,139,133,213,160,229.3C186.7,245,213,203,240,202.7C266.7,203,293,245,320,256C346.7,267,373,245,400,250.7C426.7,256,453,288,480,277.3C506.7,267,533,213,560,213.3C586.7,213,613,267,640,256C666.7,245,693,171,720,138.7C746.7,107,773,117,800,133.3C826.7,149,853,171,880,202.7C906.7,235,933,277,960,288C986.7,299,1013,277,1040,261.3C1066.7,245,1093,235,1120,218.7C1146.7,203,1173,181,1200,170.7C1226.7,160,1253,160,1280,181.3C1306.7,203,1333,245,1360,272C1386.7,299,1413,309,1427,314.7L1440,320L1440,320L1426.7,320C1413.3,320,1387,320,1360,320C1333.3,320,1307,320,1280,320C1253.3,320,1227,320,1200,320C1173.3,320,1147,320,1120,320C1093.3,320,1067,320,1040,320C1013.3,320,987,320,960,320C933.3,320,907,320,880,320C853.3,320,827,320,800,320C773.3,320,747,320,720,320C693.3,320,667,320,640,320C613.3,320,587,320,560,320C533.3,320,507,320,480,320C453.3,320,427,320,400,320C373.3,320,347,320,320,320C293.3,320,267,320,240,320C213.3,320,187,320,160,320C133.3,320,107,320,80,320C53.3,320,27,320,13,320L0,320Z"
          ></path>
        </svg>
      </header>

      <section class="services">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-10 g-3 my-4">
              <h1> {t("Despre4.t")} </h1>
              <p>{t("Despre5.t")}</p>
              <p>{t("Despre6.t")}</p>
            </div>
            <div class="col-md-10 mb-5">
              <img src={Abt} alt="" class="img-fluid" />
            </div>
            <div class="col-md-10 g-3 my-4">
              <h1> {t("Despre7.t")} </h1>
            </div>
          </div>
        </div>
      </section>

      <footer class="page-footer gradient">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fill-opacity="1"
            d="M0,0L40,21.3C80,43,160,85,240,117.3C320,149,400,171,480,176C560,181,640,171,720,149.3C800,128,880,96,960,74.7C1040,53,1120,43,1200,48C1280,53,1360,75,1400,85.3L1440,96L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          ></path>
        </svg>
      </footer>
    </div>
  );
}
