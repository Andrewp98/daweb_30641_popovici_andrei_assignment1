import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import twh from "./img/teeth_whitening.svg";
import Aug from "./img/Serv/augmentare.jpg";
import Chr from "./img/Serv/chirurgie.jpg";
import Endo from "./img/Serv/endodontie.jpg";
import Odon from "./img/Serv/odontologie.jpg";
import Estden from "./img/Serv/estetica-dentara.jpg";
import Implt from "./img/Serv/implantologie.jpg";
import Prflx from "./img/Serv/profilaxie.jpg";
import Prote from "./img/Serv/protetica.jpg";
import Orto from "./img/Serv/ortodontie.jpg";

export default function Srvicii() {
  const { t, i18n } = useTranslation();

  return (
    <div className="servicii">
      <header class="page-header gradient">
        <div class="container pt-3">
          <div class="row allign-items-center justify-content-center">
            <div class="col-md-5">
              <img src={twh} alt="Header image" />
            </div>
            <div class="col-md-5">
              <h2 class="my-4">{t("Servicii1.t")}</h2>

              <div class="container">
                <div class="row align-items-center my-3 g-3">
                  <div class="col-md-6">
                    <ul>
                      <li>{t("Servicii2.t")}</li>
                      <li>{t("Servicii3.t")}</li>
                      <li>{t("Servicii4.t")}</li>
                      <li>{t("Servicii5.t")}</li>
                      <li>{t("Servicii6.t")}</li>
                      <li>{t("Servicii7.t")}</li>
                      <li>{t("Servicii8.t")}</li>
                      <li>{t("Servicii9.t")}</li>
                      <li>{t("Servicii10.t")}</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fill-opacity="1"
            d="M0,32L26.7,32C53.3,32,107,32,160,53.3C213.3,75,267,117,320,122.7C373.3,128,427,96,480,106.7C533.3,117,587,171,640,192C693.3,213,747,203,800,176C853.3,149,907,107,960,85.3C1013.3,64,1067,64,1120,69.3C1173.3,75,1227,85,1280,96C1333.3,107,1387,117,1413,122.7L1440,128L1440,320L1413.3,320C1386.7,320,1333,320,1280,320C1226.7,320,1173,320,1120,320C1066.7,320,1013,320,960,320C906.7,320,853,320,800,320C746.7,320,693,320,640,320C586.7,320,533,320,480,320C426.7,320,373,320,320,320C266.7,320,213,320,160,320C106.7,320,53,320,27,320L0,320Z"
          ></path>
        </svg>
      </header>

      <section class="servicii">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-5">
              <h1>{t("Servicii2.t")}</h1>

              <p>{t("Servicii11.t")}</p>
            </div>
            <div class="col-md-5">
              <img src={Aug} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <img src={Chr} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <h1>{t("Servicii3.t")}</h1>

              <p>{t("Servicii12.t")}</p>
            </div>
            <div class="col-md-5 g-3 my-4">
              <h1>{t("Servicii4.t")}</h1>

              <p>{t("Servicii13.t")}</p>
            </div>
            <div class="col-md-5">
              <img src={Endo} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <img src={Odon} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <h1>{t("Servicii5.t")}</h1>

              <p>{t("Servicii14.t")}</p>
            </div>
            <div class="col-md-5">
              <h1>{t("Servicii6.t")}</h1>

              <p>{t("Servicii15.t")}</p>
            </div>
            <div class="col-md-5">
              <img src={Estden} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <img src={Implt} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <h1>{t("Servicii7.t")}</h1>

              <p>{t("Servicii16.t")}</p>
            </div>
            <div class="col-md-5">
              <h1>{t("Servicii8.t")}</h1>

              <p>{t("Servicii17.t")}</p>
            </div>
            <div class="col-md-5">
              <img src={Prflx} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <img src={Prote} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <h1>{t("Servicii9.t")}</h1>

              <p>{t("Servicii18.t")}</p>
            </div>
            <div class="col-md-5">
              <h1>{t("Servicii10.t")}</h1>

              <p>{t("Servicii19.t")}</p>
            </div>
            <div class="col-md-5">
              <img src={Orto} alt="" class="img-fluid" />
            </div>
          </div>
        </div>
      </section>

      <footer class="page-footer gradient">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fill-opacity="1"
            d="M0,0L40,21.3C80,43,160,85,240,117.3C320,149,400,171,480,176C560,181,640,171,720,149.3C800,128,880,96,960,74.7C1040,53,1120,43,1200,48C1280,53,1360,75,1400,85.3L1440,96L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          ></path>
        </svg>
      </footer>
    </div>
  );
}
