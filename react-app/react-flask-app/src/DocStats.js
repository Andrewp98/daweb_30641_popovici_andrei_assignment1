import React, { useState, useEffect, useContext, createContext } from "react";
import { Bar } from "react-chartjs-2";
import { Line } from "react-chartjs-2";
import axios from "axios";
import { useUserContext } from "./hooks/UserContext";
import { useHistory } from "react-router-dom";

const DocStats = () => {
  const [chartData, setChartData] = useState({});
  const [appDate, setAppDate] = useState([]);
  const [appCount, setAppCount] = useState([]);

  const { state: userState } = useUserContext();
  const history = useHistory();

  let dateMap = new Map();

  const chart = () => {
    let app_date = [];
    let app_count = [];

    const urlText =
      "http://localhost:8000/api/getAppoints/" + userState.user?.id;

    axios({
      method: "get",
      url: urlText,
    })
      .then(function (response) {
        //handle success
        response.data.data.map((entry) => {
          // console.log(entry.patientId + " " + entry.appDate)
          if (dateMap.has(entry.appDate)) {
            //crestem contorul de aparitii
            dateMap.set(entry.appDate, dateMap.get(entry.appDate) + 1);
          } else {
            dateMap.set(entry.appDate, 1);
          }
          //   setAppointNames((prevArray) => [...prevArray, entry.appDate]);
          // console.log(appointNames);
        });
        //datele din map
        [...dateMap.entries()].map((entry) => {
          app_date.push(entry[0]);
          app_count.push(entry[1]);
        });

        console.log(app_date);
        console.log(app_count);

        setChartData({
          labels: app_date,
          datasets: [
            {
              label: "Numatul de programari pe zile",
              data: app_count,
              backgroundColor: ["rgba(75, 192, 192, 0.6)"],
              borderWidth: 4,
            },
          ],
        });
      })
      .catch((error) => {
        console.log("ERRRR:: ", error.response.data);
      });
  };

  useEffect(() => {
    chart();
  }, []);

  return (
    <div className="App">
      <h1>Appointment chart</h1>

      <div>
        <Bar
          data={chartData}
          options={{
            responsive: true,
            title: { text: "Numatul de programari", display: true },
            scales: {
              yAxes: [
                {
                  ticks: {
                    autoSkip: true,
                    maxTicksLimit: 10,
                    beginAtZero: true,
                  },
                  gridLines: {
                    display: false,
                  },
                },
              ],
              xAxes: [
                {
                  gridLines: {
                    display: false,
                  },
                },
              ],
            },
          }}
        />
      </div>

      <div style={{ float: "right" }}>
        <button
          type="button"
          className="btn btn-outline-secondary mx-3"
          onClick={() => {
            history.push("/doctorspage");
          }}
        >
          Go back
        </button>
      </div>
    </div>
  );
};

export default DocStats;
