import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import backgroundImage from "./img/Cover.jpg";

export default function Contact() {
  const { t, i18n } = useTranslation();
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const submitRequest = async (e) => {
    e.preventDefault();
    console.log({ email, message });
    const response = await fetch("/contact", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({ email, message }),
    });

    const resData = await response.json();
    if (resData.status === "success") {
      alert("Message Sent.");
      this.resetForm();
    } else if (resData.status === "fail") {
      alert("Message failed to send.");
    }
  };

  return (
    <div className="contact">
      <header className="page=header gradient">
        <div
          style={{
            backgroundImage: "url(" + backgroundImage + ")",
            height: "958px",
            marginTop: "58px",
          }}
          className="p-5 text-center bg-image"
        >
          <div
            className="mask"
            style={{ backgroundColor: "rgba(0, 0, 0, 0.6)" }}
          >
            <div className="d-flex justify-content-center align-items-center h-100">
              <div className="text-white">
                <h1 className="mb-3 mt-3">{t("Contact1.t")}</h1>
                <h4 className="mb-3">{t("Contact2.t")}</h4>
                <a
                  className="btn btn-outline-light btn-lg mb-4"
                  href="#here"
                  role="button"
                >
                  {t("Contact3.t")}
                </a>
              </div>
            </div>
          </div>
        </div>
      </header>

      <section className="contact">
        <div className="container">
          <div className="row">
            <form
              className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 mt-4 border-gray-200 border"
              onSubmit={submitRequest}
            >
              <div className="mb-4 mt-4 ">
                <h1>{t("Contact4.t")}</h1>
                <div className="mb-3" id="here">
                  <label for="exampleFormControlInput1" className="form-label">
                    {t("Contact5.t")}
                  </label>
                  <input
                    type="text"
                    name="email"
                    className="form-control"
                    id="exampleFormControlInput1"
                    placeholder="name@example.com"
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    required
                  />
                </div>
                <div className="mb-3">
                  <label
                    for="exampleFormControlTextarea1"
                    className="form-label"
                  >
                    {t("Contact6.t")}
                  </label>
                  <textarea
                    className="form-control"
                    id="exampleFormControlTextarea1"
                    name="message"
                    type="text"
                    placeholder={t("Contact7.t")}
                    onChange={(e) => setMessage(e.target.value)}
                    value={message}
                    required
                  ></textarea>
                </div>
                <button type="submit" className="btn btn-primary">
                  {t("Contact8.t")}
                </button>
              </div>
            </form>
            <div className="col-md-5">
              <img src="img/hand.svg" alt="" />
            </div>
          </div>
        </div>
      </section>

      <footer className="page-footer gradient">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fill-opacity="1"
            d="M0,0L40,21.3C80,43,160,85,240,117.3C320,149,400,171,480,176C560,181,640,171,720,149.3C800,128,880,96,960,74.7C1040,53,1120,43,1200,48C1280,53,1360,75,1400,85.3L1440,96L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          ></path>
        </svg>
      </footer>
    </div>
  );
}
