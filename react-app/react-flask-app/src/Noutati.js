import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import Iframe from "react-iframe";
import Carousel from "react-bootstrap/Carousel";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import Rev from "./img/rev.svg";
import Rad from "./img/Noutati/rad.JPG";
import Kid from "./img/Noutati/kid.JPG";
import Toofth from "./img/Noutati/toofth.JPG";

function Noutati() {
  const { t, i18n } = useTranslation();

  return (
    <div className="Noutati">
      <header class="page-header gradient">
        <div class="container pt-3">
          <div class="row allign-items-center justify-content-center">
            <div class="col-md-5">
              <img src={Rev} alt="Header image" />
            </div>
            <div class="col-md-5">
              <h2 class="my-4">{t("Noutati1.t")}</h2>

              <p>{t("Noutati2.t")}</p>
            </div>
          </div>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fill-opacity="1"
            d="M0,0L26.7,16C53.3,32,107,64,160,80C213.3,96,267,96,320,90.7C373.3,85,427,75,480,69.3C533.3,64,587,64,640,90.7C693.3,117,747,171,800,165.3C853.3,160,907,96,960,64C1013.3,32,1067,32,1120,53.3C1173.3,75,1227,117,1280,144C1333.3,171,1387,181,1413,186.7L1440,192L1440,320L1413.3,320C1386.7,320,1333,320,1280,320C1226.7,320,1173,320,1120,320C1066.7,320,1013,320,960,320C906.7,320,853,320,800,320C746.7,320,693,320,640,320C586.7,320,533,320,480,320C426.7,320,373,320,320,320C266.7,320,213,320,160,320C106.7,320,53,320,27,320L0,320Z"
          ></path>
        </svg>
      </header>

      <section class="services">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-10 g-3 my-4">
              <h1> {t("Noutati3.t")} </h1>
              <p>{t("Noutati4.t")}</p>
            </div>
            <div class="col-md-10 mb-5">
              <img src={Rad} alt="" class="img-fluid" />
            </div>

            <div class="col-md-10 g-3 my-4">
              <h1>{t("Noutati5.t")}</h1>
              <p>{t("Noutati6.t")}</p>
            </div>
            <div class="col-md-10">
              <img src={Kid} alt="" class="img-fluid" />
            </div>

            <div class="col-md-10 g-3 my-4">
              <h1>{t("Noutati7.t")}</h1>
              <p>{t("Noutati8.t")}</p>
            </div>
            <div class="col-md-10 mb-5">
              <img src={Toofth} alt="" class="img-fluid" />
            </div>
            <h1>{t("Noutati9.t")}</h1>
          </div>
        </div>
      </section>

      <Carousel fade>
        <Carousel.Item>
          <Iframe
            url="https://www.youtube.com/embed/3wvVrkMKim8"
            width="100%"
            height="580px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative"
          />
          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Iframe
            url="https://www.youtube.com/embed/eZDwzYhbIBU"
            width="100%"
            height="580px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative"
          />

          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Iframe
            url="https://www.youtube.com/embed/AATnG9BdIMg"
            width="100%"
            height="580px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative"
          />

          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
      </Carousel>

      <footer class="page-footer gradient">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fill-opacity="1"
            d="M0,0L40,21.3C80,43,160,85,240,117.3C320,149,400,171,480,176C560,181,640,171,720,149.3C800,128,880,96,960,74.7C1040,53,1120,43,1200,48C1280,53,1360,75,1400,85.3L1440,96L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          ></path>
        </svg>
      </footer>
    </div>
  );
}

export default Noutati;
