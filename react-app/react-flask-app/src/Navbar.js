import React from "react";

import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";

const Navbar = (props) => {
  const { t, i18n } = useTranslation();
  const history = useHistory();

  function handleClick(lang) {
    i18n.changeLanguage(lang);
  }

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <div className="container">
        <div className="langAndLogo">
          <button onClick={() => handleClick("en")}>EN</button>
          <button onClick={() => handleClick("ro")}>RO</button>
          <a className="navbar-brand" href={"/"}>
            DentalWEB
          </a>
        </div>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="collapse navbar-collapse justify-content-end"
          id="navbarNav"
        >
          <ul className="navbar-nav">
            <li className="nav-item">
              <a
                className="nav-link active"
                aria-current="page"
                onClick={() => {
                  history.push("/");
                }}
              >
                {t("Acasa.title")}
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                onClick={() => {
                  history.push("/noutati");
                }}
              >
                {t("Noutati.title")}
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                onClick={() => {
                  history.push("/despre");
                }}
              >
                {t("Despre.title")}
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                onClick={() => {
                  history.push("/medici");
                }}
              >
                {t("Medici.title")}
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                onClick={() => {
                  history.push("/servicii");
                }}
              >
                {t("Servicii.title")}
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                onClick={() => {
                  history.push("/contact");
                }}
              >
                {t("Contact.title")}
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                onClick={() => {
                  history.push("/login");
                }}
              >
                Login
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                onClick={() => {
                  history.push("/signup");
                }}
              >
                Sign Up
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
