import axios from "axios";
import React, { useState } from "react";
import { Form, FormGroup, Label, Input, Col } from "reactstrap";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import { useUserContext } from "./hooks/UserContext";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation,
} from "react-router-dom";

export default function Login() {
  const [email, setEmail] = useState([]);
  const [password, setPassword] = useState([]);
  const { state: userState } = useUserContext();
  const history = useHistory();

  const { dispatch } = useUserContext();

  const handleLogin = () => {
    axios
      .post("http://localhost:8000/api/login", {
        email: email,
        password: password,
      })
      .then((res) => {
        console.log(res.data.data);
        dispatch({ type: "login", payload: res.data.data });
        if (res.data.data.role === "user") {
          history.push("/userpage");
        } else if (res.data.data.role === "med") {
          history.push("/doctorspage");
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="loginform">
      <header className="page-header gradient">
        <div className="container pt-3">
          <div className="row allign-items-center justify-content-center"></div>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#ffffff"
            fillOpacity="1"
            d="M0,288L48,272C96,256,192,224,288,218.7C384,213,480,235,576,202.7C672,171,768,85,864,69.3C960,53,1056,107,1152,117.3C1248,128,1344,96,1392,80L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
          ></path>
        </svg>
      </header>

      <section className="loginContainer">
        <div className="container">
          <div className="row allign-items-center justify-content-center">
            <form className="bg-white shadow-md rounded w-50 px-8 pt-6 pb-8 mb-4 mt-4 border-gray-200 border">
              <div className="mb-4 mt-4 ">
                <h1>Sign In</h1>

                <div className="mb-3" id="here">
                  <label for="exampleFormControlInput1" className="form-label">
                    Email address
                  </label>
                  <input
                    type="text"
                    name="email"
                    className="form-control"
                    id="exampleFormControlInput1"
                    placeholder="name@example.com"
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    required
                  />
                </div>

                <div className="mb-3">
                  <label
                    for="exampleFormControlTextarea1"
                    className="form-label"
                  >
                    Password
                  </label>
                  <input
                    className="form-control"
                    id="exampleFormControlTextarea1"
                    name="password"
                    type="password"
                    placeholder="Enter password"
                    onChange={(e) => setPassword(e.target.value)}
                    value={password}
                    required
                  />
                </div>

                <div className="mb-3">
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck1"
                    />
                    <label
                      className="custom-control-label px-1"
                      htmlFor="customCheck1"
                    >
                      Remember me
                    </label>
                  </div>
                </div>

                <div className="mb-3 ">
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={handleLogin}
                  >
                    Login
                  </button>

                  <p className="text-right">
                    Forgot <a href="#">password?</a>
                  </p>
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>

      <footer className="page-footer gradient">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fillOpacity="1"
            d="M0,0L40,21.3C80,43,160,85,240,117.3C320,149,400,171,480,176C560,181,640,171,720,149.3C800,128,880,96,960,74.7C1040,53,1120,43,1200,48C1280,53,1360,75,1400,85.3L1440,96L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          ></path>
        </svg>
      </footer>
    </div>
  );
}
