import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import Noutati from "./Noutati";
import Contact from "./Contact";
import Despre from "./Despre";
import Medici from "./Medici";
import Servicii from "./Servicii";
import Home from "./Home";
import Login from "./LoginComp";
import SignUp from "./SignupComp";
import Navbar from "./Navbar";
import Userpage from "./UserPage";
import DoctorsPage from "./DoctorsPage";
import DocStats from "./DocStats";
import { UserProvider } from "./hooks/UserContext";

function App(props) {
  return (
    <UserProvider>
      <div className="App">
        <Router>
          <Navbar />
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/noutati">
              <Noutati />
            </Route>
            <Route exact path="/contact">
              <Contact />
            </Route>
            <Route exact path="/despre">
              <Despre />
            </Route>
            <Route exact path="/medici">
              <Medici />
            </Route>
            <Route exact path="/servicii">
              <Servicii />
            </Route>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route exact path="/signup">
              <SignUp />
            </Route>
            <Route exact path="/userpage">
              <Userpage />
            </Route>
            <Route exact path="/doctorspage">
              <DoctorsPage />
            </Route>
            <Route exact path="/docStats">
              <DocStats />
            </Route>
          </Switch>
        </Router>
      </div>
    </UserProvider>
  );
}

export default App;
