import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import "./css/bootstrap.min.css";
import "./css/styles.css";
import D1 from "./img/Docs/d1.jpg";
import D2 from "./img/Docs/d2.jpg";
import D3 from "./img/Docs/d3.jpg";

export default function Medici() {
  const { t, i18n } = useTranslation();

  return (
    <div className="medici">
      <header class="page-header gradient">
        <div class="container pt-3">
          <div class="col-md-10">
            <h2 class="my-4">{t("Medici1.t")}</h2>
            <p>{t("Medici2.t")}</p>
          </div>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fill-opacity="1"
            d="M0,64L40,85.3C80,107,160,149,240,160C320,171,400,149,480,122.7C560,96,640,64,720,90.7C800,117,880,203,960,213.3C1040,224,1120,160,1200,128C1280,96,1360,96,1400,96L1440,96L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"
          ></path>
        </svg>
      </header>

      <section class="services">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-5">
              <button type="button" class="btn btn-outline-warning mb-3">
                Contact
              </button>

              <h1>Dr. Viorel Truta</h1>

              <p>{t("Medici3.t")}</p>
            </div>
            <div class="col-md-5">
              <img src={D1} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <img src={D2} alt="" class="img-fluid" />
            </div>
            <div class="col-md-5">
              <button type="button " class="btn btn-outline-success mb-3">
                Contact
              </button>

              <h1>Dr. Raluca Zimbru</h1>

              <p>{t("Medici4.t")}</p>
            </div>
            <div class="col-md-5 g-3 my-4">
              <button type="button" class="btn btn-outline-dark mb-3">
                Contact
              </button>

              <h1>Dr. Dominica Bouchamp</h1>

              <p>{t("Medici5.t")}</p>
            </div>
            <div class="col-md-5">
              <img src={D3} alt="" class="img-fluid" />
            </div>
          </div>
        </div>
      </section>

      <footer class="page-footer gradient">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#fff"
            fill-opacity="1"
            d="M0,0L40,21.3C80,43,160,85,240,117.3C320,149,400,171,480,176C560,181,640,171,720,149.3C800,128,880,96,960,74.7C1040,53,1120,43,1200,48C1280,53,1360,75,1400,85.3L1440,96L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          ></path>
        </svg>
      </footer>
    </div>
  );
}
